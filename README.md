# Floorplan Orientation

This repository contains Jupyter notebooks to illustrate various algorithms for orienting a floorplan image with a building via connected reference points. The floorplan could be a [low-quality scanned paper floorplan](floorplan_orig.jpg) posted in public buildings for emergency purposes. The building is represented with a [GeoJSON object](cepsr_detail.png) obtained from OpenStreetMap. The orientation algorithm produces a [transformed floorplan image that can be overlaid over a map view](cepsr_floorplan_superimposed.png).

## Notebooks

1. The [first notebook](geo.ipynb) illustrates simple orientation based on translation, rotation, and scaling
1. The [second notebook](geo2.ipynb) illustrates floorplan orientation based on similarity and affine transformations
1. The [third notebook](geo3.ipynb) illustrates floorplan orientation based on a projective transformation
