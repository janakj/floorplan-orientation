import matplotlib
matplotlib.rcParams['figure.dpi']= 200
import matplotlib.pyplot as plt
import numpy as np
import collections
import math

Point = collections.namedtuple('Point', 'x y')
RefPoint = collections.namedtuple('RefPoint', 'old new')


def get_corners(rect):
    br = Point(x=rect.ur.x, y=rect.bl.y)
    ul = Point(x=rect.bl.x, y=rect.ur.y)
    return [rect.bl, br, rect.ur, ul]


def polygon(polyline):
    '''Convert polyline to a (closed) polygon by ensuring that the first and last point are the same.
    '''
    
    # Append the polygon's first point at the end of the list to close the shape
    polygon = polyline[:]
    
    if polygon[-1] != polygon[0]:
        polygon.append(polygon[0])
        
    return polygon


def plot(polyline):
    # Return two lists where the first list contains x coordinates and the second
    # list contains y coordinates
    return ([p.x for p in polyline], [p.y for p in polyline])


def draw(poly, color="red", top=2000, bottom=-500, left=-500, right=4000, tick=500, point=None):
    fig, axes = plt.subplots()

    plt.gca().yaxis.set_major_locator(plt.MultipleLocator(tick))
    plt.gca().xaxis.set_major_locator(plt.MultipleLocator(tick))
    plt.ylim(top=top, bottom=bottom)
    plt.xlim(left=left,right=right)
    plt.grid()

    axes.set_aspect('equal')
    axes.set(xlabel='Width [px]', ylabel='Height [px]')
        
    plt.plot(*plot(polygon(poly)), color=color, zorder=20)
    
    if point:
        plt.plot(point.x, point.y, color="blue", marker='x', markersize=3)
    
    plt.show()
    
    
def _distance(a, b):
    '''Find euclidian distance between points 'a' and 'b'
    '''
    return ((b.x - a.x)**2 + (b.y - a.y)**2)**0.5


def scale(ref_a, ref_b):
    '''Find the scaling factor using two reference points 'ref_a' and 'ref_b'
    '''
    old_distance = _distance(ref_a.old, ref_b.old)
    new_distance = _distance(ref_a.new, ref_b.new)
    return new_distance / old_distance


def _middle(a, b):
    return (b - a) / 2 + a


def _line_center(a, b):
    return Point(x=_middle(a.x, b.x), y=_middle(a.y, b.y))


def translate(ref_a, ref_b):
    old_center = _line_center(ref_a.old, ref_b.old)
    new_center = _line_center(ref_a.new, ref_b.new)
    return (new_center.x - old_center.x, new_center.y - old_center.y)


def _angle(width, height):
    return math.atan(height / width)


def rotate(ref_a, ref_b):
    old_angle = _angle(ref_b.old.x - ref_a.old.x, ref_b.old.y - ref_a.old.y)
    new_angle = _angle(ref_b.new.x - ref_a.new.x, ref_b.new.y - ref_a.new.y)
    return new_angle - old_angle;


def polyline2array(polyline):
    return np.array(polyline).reshape((len(polyline), 2))
